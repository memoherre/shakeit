package mx.itesm.shakeit;

import android.app.Application;

public class GlobalValues extends Application {

    private String resName,
                   promo,
                   lati,
                   longi,
                   cupID,
                   key;

    public String getKey(){ return key; }

    public String getResName(){
        return resName;
    }

    public String getCupID() {
        return cupID;
    }

    public String getLati() {
        return lati;
    }

    public String getLongi() {
        return longi;
    }

    public String getPromo() {
        return promo;
    }

    public void setCupID(String cupID) {
        this.cupID = cupID;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public void setPromo(String promo) {
        this.promo = promo;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public void setKey(String key){this.key = key;}
}
