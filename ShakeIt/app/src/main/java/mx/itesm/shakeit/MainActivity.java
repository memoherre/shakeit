package mx.itesm.shakeit;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.tbouron.shakedetector.library.ShakeDetector;

public class MainActivity extends Activity{
    private TextView tvHello;
    private Button bCupones, bShare;
    private Intent intent;
    private String shareBody = "Prueba la app ShakeIt para poder obtener descuentos. Descargala en www.linkparadescargarshakeit.com";
    private SensorManager sensorMgr;


    private static String DEFAULT_CHANNEL_ID = "default_channel";
    private static String DEFAULT_CHANNEL_NAME = "Default";
    private NotificationManager mNotificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvHello = findViewById(R.id.tvHello);
        bCupones = findViewById(R.id.bCupones);

        Intent i = getIntent();
        tvHello.setText("Hello " + i.getStringExtra("username"));

        ShakeDetector.create(this, new ShakeDetector.OnShakeListener() {

            @Override
            public void OnShake() {
                Toast.makeText(getApplicationContext(), "Lo shakeamos!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(MainActivity.this, CuponListItem.class);
                // Intent i = new Intent(MainActivity.this, ListaCupones.class);
                startActivity(i);
            }
        });

        bShare = findViewById(R.id.bShare);
        bShare.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My App");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share via"));
            }
        });

    }

    public void changeToListaCupones(View v){
        Intent i = new Intent(MainActivity.this, CuponListItem.class);
        // Intent i = new Intent(MainActivity.this, ListaCupones.class);
        startActivity(i);
    }


    /*
     * Create NotificationChannel as required from Android 8.0 (Oreo)
     * */
    public static void createNotificationChannel(NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Create channel only if it is not already created
            if (notificationManager.getNotificationChannel(DEFAULT_CHANNEL_ID) == null) {
                notificationManager.createNotificationChannel(new NotificationChannel(
                        DEFAULT_CHANNEL_ID, DEFAULT_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
                ));
            }
        }
    }

    public void showNotification(View v){
        //1.Get reference to Notification Manager
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createNotificationChannel(mNotificationManager);

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //2.Build Notification with NotificationCompat.Builder
        Notification notification = new NotificationCompat.Builder(this, DEFAULT_CHANNEL_ID)
                .setContentTitle("Get coupon")   //Set the title of Notification
                .setContentText("Use ShakeIt to get great coupons!")    //Set the text for notification
                .setSmallIcon(android.R.drawable.ic_menu_view)   //Set the icon
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        //Send the notification.
        mNotificationManager.notify(1, notification);
    }


    @Override
    protected void onResume() {
        super.onResume();
        ShakeDetector.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ShakeDetector.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShakeDetector.destroy();
    }
}
