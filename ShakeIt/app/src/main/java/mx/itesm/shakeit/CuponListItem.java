package mx.itesm.shakeit;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class CuponListItem extends AppCompatActivity {

    private Button b, bShareCupon;
    private RecyclerView mRecyclerView;
    DatabaseReference myRef;
    private String prom,
                   name,
                   latt,
                   lngg,
                   cupId,
                   key;
    private Intent intent;
    private String shareBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cupon_list_item2);

        b = findViewById(R.id.bMapACLI);
        bShareCupon = findViewById(R.id.bShareCupon);
        mRecyclerView = findViewById(R.id.recyclerView);

        dbCon();
    }

    public void dbCon(){
        new FirebaseDatabaseHelper().leerCupones(new FirebaseDatabaseHelper.DataStatus() {
            @Override
            public void DataIsLoaded(List<Cupon> cupones, List<String> keys, List<Location> locations) {
                new RecyclerView_Config().setConfig(mRecyclerView, CuponListItem.this, cupones, keys, locations);
            }

            @Override
            public void DataIsInserted() {

            }

            @Override
            public void DataIsUpdated() {

            }

            @Override
            public void DataIsDeleted() {

            }
        });
    }

    public void loadElements(){

        GlobalValues gb = (GlobalValues) this.getApplicationContext();

        this.name = gb.getResName();
        this.prom = gb.getPromo();
        this.latt = gb.getLati();
        this.lngg = gb.getLongi();
        this.cupId= gb.getCupID();
        this.key = gb.getKey();

        String msj = "Name : "+this.name+
                " Prom :"+this.prom+
                " Lat :"+this.latt+
                " Log :"+this.lngg+
                " ID :"+this.cupId+
                " Key :"+this.key;

        Log.wtf("Mensajito ",msj);

    }

    public void changeMap(View v){

        loadElements();
        Intent i = new Intent(this, MapsActivity.class);

        /*
        i.putExtra("latitud", "20.7346349");
        i.putExtra("longitud", "-103.4555114");
        i.putExtra("restaurante", "Starbucks Tec");
        */

        i.putExtra("latitud", this.latt);
        i.putExtra("longitud", this.lngg);
        i.putExtra("nombre", this.name);

        startActivity(i);

    }

    public void usarCupon(View v){
        loadElements();

        myRef = FirebaseDatabase.getInstance().getReference().child("Locations/" + this.key + "/Cupones");
        myRef.child(this.cupId).removeValue();
        Toast.makeText(this,"Cupón usado: "+this.name,Toast.LENGTH_SHORT).show();
        Intent i = new Intent(CuponListItem.this, MainActivity.class);
        startActivity(i);

    }

    public void share(View v){
        loadElements();
        shareBody = this.name + "de" + this.prom + "";
        intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My App");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(intent, "Share via"));
    }
}
